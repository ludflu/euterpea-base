module Main where

import Euterpea (Music ((:+:), (:=:)), play, en, qn, wn, rest, line, PitchClass(A), Pitch, writeMidi)
import Scales
import Permute
import Data.List.Split


makeMelody :: [[Int]] -> Pitch -> Mood -> [Music Pitch]
makeMelody degrees key mood = let noteFetcher = getNotes key mood qn
                               in map (line . noteFetcher) degrees 

makeSong :: PitchClass -> Mood -> [[Int]] -> Progression -> Music Pitch
makeSong key mood perms (Progression progs) = let mixem = permutes perms
                                                  mixed = take 20 $ iterate mixem [0..6]
                                                  mels = chunksOf 4 $ concat mixed
                                                  melody = makeMelody mels (key,4) mood
                                                  chordRotations = concat $ take 10 $ iterate rotateL progs
                                                  scalePattern = moodToPattern mood
                                                  chrdds = makeChordProgression chordRotations (key,3) scalePattern
                                                  measures = zipWith (:=:) melody chrdds  --play the chords with the melod
                                                  song = foldl (:+:) (rest en) measures   --play the measures one after the other
                                               in song


-- defines a permutation in cycle notation
-- https://en.wikipedia.org/wiki/Cyclic_permutation
-- to create a melody by applying the permutation to the selected scale
permutation = [[2,3],[2,1,5],[5,7,0,8]]

--define a chord progression
progression = Progression [(I,Major), (V, Major), (VI,Minor), (IV,Major)]

song = makeSong A Major permutation progression

main :: IO ()
main = do --writeMidi "/tmp/song.mid" song
          print song
          play song
